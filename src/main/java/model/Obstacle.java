package model;

import org.apache.commons.lang3.tuple.Pair;
import org.dyn4j.dynamics.*;
import org.dyn4j.geometry.*;

public class Obstacle {

    private Body physicalBody;
    private Pair<Double, Double> position;
    private double width;
    private double height;
    private double angle; //degree
    
    public Obstacle(double width, double height, double angle, Pair<Double, Double> position) {
        //other shit
        this.position = position;
        this.width = width;
        this.height = height;
        this.angle = angle;
        //dyn4j shit
        this.physicalBody = new Body();
        BodyFixture fixture = new BodyFixture(new Rectangle(this.width, this.height));
        fixture.setRestitution(1);
        this.physicalBody.addFixture(fixture);
        this.physicalBody.setMass(MassType.INFINITE); //makes it stand still
        this.physicalBody.translateToOrigin();
        this.physicalBody.translate(this.position.getLeft(), this.position.getRight());
        this.physicalBody.rotateAboutCenter(this.angle*Math.PI/180);
    }
    
    protected void update() {
        
    }
    
    protected Body getPhysicalBody() {
        return this.physicalBody;
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public double getHeight() {
        return this.height;
    }
    
    public double getAngle() {
        return this.angle;
    }
    
    public Pair<Double, Double> getPosition() {
        return this.position;
    }
    
}
