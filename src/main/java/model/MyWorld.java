package model;

import java.util.LinkedList;
import java.util.List;

import org.dyn4j.dynamics.World;

public class MyWorld {

    private World physicalWorld;
    private Ball ball;
    private List<Obstacle> obstacles;
    
    public MyWorld() {
        this.physicalWorld = new World();
//        this.physicalWorld.set
        this.obstacles = new LinkedList<Obstacle>();
    }
    
    public void update(double elapsedTime) {
        this.physicalWorld.update(elapsedTime);
        this.ball.update();
        this.obstacles.forEach(o -> o.update());
    }
    
    public void addBall(Ball b) {
        this.ball = b;
        this.physicalWorld.addBody(b.getPhysicalBody());
    }
    
    public void addObstacle(Obstacle o) {
        this.obstacles.add(o);
        this.physicalWorld.addBody(o.getPhysicalBody());
    }
    
    public Ball getBall() {
        return this.ball;
    }
    
    public List<Obstacle> getObastacles() {
        return this.obstacles;
    }

}
