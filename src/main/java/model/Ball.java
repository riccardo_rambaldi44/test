package model;

import org.apache.commons.lang3.tuple.Pair;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Transform;
import org.dyn4j.geometry.Vector2;

public class Ball {

    private Body physicalBody;
    private Pair<Double, Double> position;
    double radius;
    
    public Ball(double radius, Pair<Double, Double> position) {
        //other shit
        this.position = position;
        this.radius = radius;
        //dyn4j shit
        this.physicalBody = new Body();
        BodyFixture fixture = new BodyFixture(new Circle(this.radius));
//        fixture.setRestitution(1);
        this.physicalBody.addFixture(fixture);
        this.physicalBody.setMass(MassType.NORMAL);
        this.physicalBody.setBullet(true);
        this.physicalBody.translate(this.position.getLeft(), this.position.getRight());
        this.physicalBody.setLinearDamping(0.5);
        this.physicalBody.getLinearVelocity().set(0, 0);
        this.physicalBody.setGravityScale(50);

    }
    
    protected void update() {
        Pair<Double, Double> previousPosition = Pair.of(this.position);
        Vector2 changeInPosition = this.physicalBody.getChangeInPosition();
        this.position = Pair.of(previousPosition.getLeft() + changeInPosition.x, previousPosition.getRight() + changeInPosition.y);
    }
    
    public void move(double x, double y) {
        Pair<Double, Double> previousPosition = Pair.of(this.position);
        this.position = Pair.of(previousPosition.getLeft() + x, previousPosition.getRight() + y);
        this.physicalBody.translate(this.position.getLeft(), this.position.getRight());
    }
    
    public void moveTo(double x, double y) {
        this.position = Pair.of(x, y);
        this.physicalBody.translateToOrigin();
        this.physicalBody.translate(x, y);    
    }
    
    protected Body getPhysicalBody() {
        return this.physicalBody;
    }

    public double getRadius() {
        return this.radius;
    }
    
    public Pair<Double, Double> getPosition() {
        return this.position;
    }
    
}
