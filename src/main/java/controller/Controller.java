package controller;

import java.awt.Toolkit;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.dyn4j.geometry.Transform;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import model.Ball;
import model.MyWorld;
import model.Obstacle;
import view.Renderer;

public class Controller {

    private static final double SCENE_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    private static final double SCENE_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
    
    private MyWorld world;
    private Renderer renderer;

    public Controller() {
        this.world = new MyWorld();
        this.renderer = new Renderer();
    }
    
    private double getRandomInBound(double min, double max) {
        return (Math.random() * ((max - min) + 1)) + min;
    }
    
    private Pair<Double, Double> getRandomPosition() {
        return Pair.of(getRandomInBound(-SCENE_WIDTH/2, SCENE_WIDTH/2), getRandomInBound(-SCENE_HEIGHT/2, 0));
    }
    
    public void inizializeWorld() {
        this.world.addBall(new Ball(5, Pair.of(0.,80.)));
        for (int i = 0; i < 300; i ++) {
            this.world.addObstacle(new Obstacle(20, 5, getRandomInBound(0, 90), getRandomPosition()));
        }
//        this.world.addObstacle(new Obstacle(100, 10, 0, Pair.of(0.,0.)));
    }
       
    public Circle getBall() {
        Ball ball = this.world.getBall();
        return this.renderer.renderBall(ball.getPosition(), ball.getRadius());
    }
    
    public List<Rectangle> getObstacles() {
        List<Obstacle> obstacles = this.world.getObastacles();
        List<Rectangle> renderedObstacles = new LinkedList<>();
        obstacles.forEach(o -> renderedObstacles.add(this.renderer.renderObstacle((ImmutablePair<Double, Double>)o.getPosition(), o.getWidth(),  o.getHeight(),  o.getAngle())));
        return renderedObstacles;
    }
    
    public void update(double elapsedTime) {
        this.world.update(elapsedTime);
    }
    
}
