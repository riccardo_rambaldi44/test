package view;

import java.awt.Toolkit;
import java.io.IOException;

import org.dyn4j.geometry.Transform;

import controller.Controller;
import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

public class AppView {

    public static final double NANO_TO_BASE = 1.0e9;
    
    private static final int SCENE_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    private static final int SCENE_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
    public static final double SCALE = 64;
    
    final private Stage stage;
    private Controller controller;
    private Circle ball;
    private Group root;
    
    protected boolean stopped;
    
    /** The time stamp for the last iteration */
    protected long last;
    
    public AppView (final Stage stage) throws IOException {
        this.controller = new Controller();
        this.controller.inizializeWorld();
        this.stage = stage;
        
        root = new Group();
        Scale s = new Scale(1, -1);
        Translate t = new Translate(SCENE_WIDTH/2, -SCENE_HEIGHT/2);
        root.getTransforms().addAll(s,t);
        
        this.drawWorld();

        Scene scene = new Scene(root, SCENE_WIDTH, SCENE_HEIGHT);

        // Stage configuration
        this.stage.setTitle("Demo");
        this.stage.setScene(scene);
        this.stage.setResizable(false);
        this.getGameLoop().start();
        
        this.stage.show();
    }
    
    private void drawWorld() {
        this.controller.getObstacles().forEach(o -> this.root.getChildren().add(o));
        this.ball = this.controller.getBall();
        this.root.getChildren().add(this.ball);
    }
    
    private void updateBall() {
//        this.root.getChildren().remove(this.ball);
//        this.ball = this.controller.getBall();
//        this.root.getChildren().add(this.ball);
        this.ball.setCenterX(this.controller.getBall().getCenterX());
        this.ball.setCenterY(this.controller.getBall().getCenterY());      
    }
    
    
    
    private AnimationTimer getGameLoop() {
        
        return new AnimationTimer() {
            
            long last = System.nanoTime();
            
            @Override
            public void handle(long now) {
                double delta = (now - last) / NANO_TO_BASE;
                controller.update(delta);
                updateBall();
//                root.getChildren().add(new Circle(MouseInfo.getPointerInfo().getLocation().getX(), MouseInfo.getPointerInfo().getLocation().getY(), 40));
                last = now;
            }
        };
    }
    
}
