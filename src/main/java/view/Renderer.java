package view;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Renderer {

    public Renderer() {
        
    }
    
    public Circle renderBall(Pair<Double, Double> position, double radius) {
        return new Circle(position.getLeft(), position.getRight(), radius, Color.BLACK);
    }
    
    public Rectangle renderObstacle(ImmutablePair<Double, Double> position, double width, double height, double angle) {
        Rectangle obstacle = new Rectangle(width, height, Color.GOLD);
        obstacle.setX(position.getLeft() - width/2);
        obstacle.setY(position.getRight() - height/2);
        obstacle.setRotate(angle);
        return obstacle;
    }
    
}
